#用户表（经理表）
CREATE TABLE IF NOT EXISTS user(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,

  #登录信息
  username VARCHAR(20) UNIQUE NOT NULL,#用户名
  password VARCHAR(20) NOT NULL ,#密码

  #联系人信息
  name VARCHAR(20) NOT NULL, #真实姓名
  cellphone CHAR(11) UNIQUE NOT NULL ,#手机号码

  #角色信息
  role TINYINT(1) DEFAULT 0,# 用户角色：0:管理员用户， 1:普通用户

  #状态信息
  status TINYINT(1) DEFAULT 0, #用户状态：0代表激活，1代表未激活
  datetime INT(10) NOT NULL
);


#客户表
CREATE TABLE IF NOT EXISTS client(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,

  #基本信息
  name VARCHAR(20) UNIQUE NOT NULL,#姓名
  gender TINYINT(1) DEFAULT 0, #性别：0代表女，1代表男
  cellphone CHAR(11) UNIQUE NOT NULL ,#手机号码

  #销售信息
  interest TINYINT(10) NOT NULL,#意向：0:高， 1:中，2：低
  status TINYINT(10) NOT NULL, #0：已定未交，1：保有，2：战败结束，3：退订，4：退车，5：待完善，
  remark VARCHAR(200) DEFAULT '',#备注
  car_id INT(10) NOT NULL,#汽车id

  #业务经理信息
  user_id INT(10) NOT NULL, #创建人id,
  datetime INT(10) NOT NULL#登记日期
);


#汽车表
CREATE TABLE IF NOT EXISTS car(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(20) NOT NULL,#汽车名字
  brand_id INT(10) NOT NULL,#汽车品牌id
  model_id INT(10) NOT NULL,#汽车型号id
  style_id INT(10) NOT NULL,#汽车款型id
  datetime INT(10) NOT NULL #添加日期
);
